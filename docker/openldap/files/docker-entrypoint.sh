#!/bin/bash

set -e


case "$1" in
    -h|--help|help)  cat /docker-entrypoint-help.txt >&2; exit 1 ;;
    slapd)           ;;
    *)               exec "$@";;
esac

mkdir -p /data/config /data/db

if [ ! -r "/data/ldif/provisioned" ]; then
    if [ -r /data/config.yml ]; then
        cp /data/config.yml /ansible/host_vars/localhost
    fi
    (cd /ansible && ansible-playbook site.yml -c local && touch /data/ldif/provisioned )
fi

if [ ! -r "/data/config/provisioned" ]; then
    slapadd -v -n 0 -l /data/ldif/database.ldif -F /data/config && touch /data/config/provisioned
fi

if [ ! -r "/data/db/provisioned" ]; then
    slapadd -v -n 1 -l /data/ldif/users-and-groups.ldif -F /data/config && touch /data/db/provisioned
fi
    
chown -R openldap:openldap /data/*

/usr/sbin/slapd -u openldap -g openldap -d3 -s trace -h "ldap://0.0.0.0:389/ ldapi://%2Fvar%2Frun%2Fslapd%2Fldapi" -F /data/config 
