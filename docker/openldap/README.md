
To build the container

    docker build --tag openldap .


To run the container:


    docker run -it -p 389:389 -e LDAP_ROOT_PASSWD=foobar -e LDAP_OPTIONS="-s trace" openldap



