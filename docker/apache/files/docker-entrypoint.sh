#!/bin/bash

set -e


case "$1" in
    -h|--help|help)  cat /docker-entrypoint-help.txt >&2; exit 1 ;;
    apache)          ;;
    *)               exec "$@";;
esac


#mkdir /var/run/apache2 /var/lock/apache2
. /etc/apache2/envvars

exec /usr/sbin/apache2ctl -D FOREGROUND


