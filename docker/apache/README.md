
# Apache 

Build new container:

    docker build --tag apache .

Run the container:

    docker run -it -p 80:80 -v $PWD:/var/www/html apache


## Strapdown

To copy strapdown and themes locally 

    cd html
    curl -L https://api.github.com/repos/arturadib/strapdown/tarball/ | tar --wildcards --strip=1 --show-transformed-names -zx */v/0.2/

